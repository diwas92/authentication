<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SuccessController extends AbstractController
{
    #[Route('/success', name: 'success')]
    public function index(): Response
    {
        return $this->render('success/index.html.twig', [
            'user' => $this->getUser(),
        ]);
    }
    #[Route('/intended', name: 'intended')]
    public function intented(): Response
    {
        return $this->render('success/intended.html.twig');
    }
}
